import React, { Component } from 'react';
 import trad from './intro/trad';


class Greetings extends Component {

    displayUser = (user) => {
        //console.log('USER 2',user);
        return <p>Welcome {user.firstName} {user.lastName}</p>
    };

    displayUsers = (users) => {

        //  return users.map((user) => <p>Welcome {user.lastName}{user.firstName}</p>);
          return (users.map ((user) => <h6>{trad.welcome[this.props.lang]} {user.lastName} {user.firstName}</h6>));
        // return (<h1>{trad.welcome[this.props.lang]} {user.lastName}{user.firstName}</h1>)   
    };

    render() {

        const user = this.props.user;
        const users = this.props.users;
        // const users = this.props.greet;

/**ce tableau
 * [{ lastName: 'Robin', firstName: 'Markusson' },
    { lastName: 'Markus', firstName: 'Robinsson' }]
 */

        return (

            <h1>
                <p>
                    Welcome{this.props.person}
                    
                    {this.displayUser(user)}

                    {/* {this.displayUsers(users)} */}

                    {this.displayUsers(users)}
                </p>
            </h1>
        );
    }
}

export default Greetings;