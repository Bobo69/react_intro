//Simplify these three functions by using the spread syntax.
//(Simplifier les trois fonctions : les réecrire pour utiliser le spread operator)


//The first one, replace(), replaces part of an array with elements from another array.
function replace(array, from, to, elements) {
    console.log(array);
    console.log(from);
    console.log(to - from);
    console.log([from, to - from].concat(elements));
    //array.splice.apply(array, [from, to - from].concat(elements));
    //array.splice.apply(array, [2, 4 - 2].concat([3, 4, 5]));
    //array.splice.apply(array, [2, 4 - 2, 3, 4, 5]);
    //array.splice(2, 4 - 2, 3, 4, 5);
    //array.splice(from, to - from, 3, 4, 5);
    array.splice(from, to - from, ...elements)


  	//HELP : array.splice.apply(array, [2, 2, 3, 4, 5] ) 
}

let testArray = [1, 2, 100, 100, 6];
replace(testArray, 2, 4, [3, 4, 5]);
console.log('replace : ', testArray);



// The second one, copyReplace, does the same,
// but creates a new array rather than modifying its argument.
function copyReplace(array, from, to, elements){
    //return array.slice(0, from).concat(elements).concat(array.slice(to))
    return [...array.slice(0, from), ...elements, ...array.slice(to)];


    
}
let testArray2 = [1, 2, 100, 100, 6];
console.log('copyReplace', copyReplace(testArray2, 2, 4, [3, 4, 5]));


//The third one is used to record a birdwatcher's sightings.
//It takes first a timestamp (say, a Date), and then any number of strings naming birds.
//It then stores these in the birdsSeen array.
let birdsSeen = [];
// function recordBirds(time) {
//     birdsSeen.push({time, birds: Array.prototype.slice.call(arguments, 1)}) 
// }
function recordBirds(time, ...birds) {
    birdsSeen.push({time, birds})
}


recordBirds(new Date(), "sparrow", "robin", "pterodactyl");
console.log('birdsSeen', birdsSeen);