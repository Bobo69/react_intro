

//Refactor the below functions using Arrow Functions

// CHALLENGE #1:
// Refactor helloWorld! with an arrow function
let helloWorld =  () => {
    console.log( "Hello World!" );
};
//Don't touch:
helloWorld();

// CHALLENGE #2:
// Oh dang, look at that HOF filtering odd numbers...
// Refactor filterOdds to a thick arrow function!
let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];
let filterOdds = (number) => 
     number % 2;


//Don't touch:
let oddNumbers = numbers.filter(filterOdds);
console.log(oddNumbers);

// CHALLENGE #3:
// Print the instance of the obj object into the console.log
let obj = {
    tick() {
        let that = this;
        setTimeout(() => { console.log(that)},1000); // Le this de let = that; sera le this de setTimeOut
    }
};

//Don't touch:
obj.tick();

// CHALLENGE #4:
// Use bind method to bind this to inline function and observe the difference
// https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Function/bind
let obj2 = {
    tick() {
        setTimeout( function () { console.log(this)}.bind(this),1000);
    }
};

//Don't touch:
obj2.tick();

// CHALLENGE #5:
// Refactor inline function into an arrow function!
let obj3 = {
    tick() {
        let that = this;
        setTimeout(function(){ console.log(that)},1000);
        setTimeout( function () { console.log(this)}.bind(this),1000);
        setTimeout( () => { console.log(this)}, 1000 );
    }
};

//Don't touch:
obj3.tick();