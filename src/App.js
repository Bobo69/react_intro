// import React from 'react';
// import logo from './logo.svg';
// import './App.css';
// //import { read } from 'fs';

// class App extends React.Component {

//   render() {
//     return (
//       <div className="App">
//         <header className="App-header">
//           <img src={logo} className="App-logo" alt="logo" />
//           <p>
//             Edit <code>src/App.js</code> and save to reload.
//         </p>
//           <a
//             className="App-link"
//             href="https://reactjs.org"
//             target="_blank"
//             rel="noopener noreferrer"
//           >
//             Learn React
//         </a>
//         </header>
//       </div>
//     );
//   }
// }

// export default App;


import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import LinkToLearn from './LinkToLearn';
import Greetings from './Greetings';
import Language from './intro/Language.js'


class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      count: 0,
      count2: 0,
      users: [{ lastName: 'Robin', firstName: 'Markusson' },                        //this.state.users
              { lastName: 'Markus', firstName: 'Robinsson' }],
      lang: 'fr'
    }

    // this.tick();

  }

  tick() {
    setInterval(() =>
      this.setState({ count: this.state.count + 1 }), 1000)
  }

  newIncrement() {
    this.setState({ count2: this.state.count2 + 1 })
  }

  newDecrement() {
    this.setState({ count2: this.state.count2 - 1 })
  }

  switchLang = (event) => {
    this.setState({ lang: event.target.value });
  }

  render() {

    const user = {lastName: 'Plon', firstName: 'Saint'};

    const people = [
      { lastName: 'Robin', firstName: 'Markusson' },
      { lastName: 'Markus', firstName: 'Robinsson' }
    ];

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
            </p>
          <LinkToLearn language={'PHP'} />
          {/* <Greetings person={'Bobo'} /> */}
          <Greetings person={'Bobo'} user={user} users={people} lang={this.state.lang} greet={this.state.users}/>
          {/* <Greetings users={people} /> */}
          {/* <Greetings lang={this.state.lang} greet={this.state.users} /> greet chehrche le tableu users dans le state */}
          <Language switchLang={this.switchLang} />
          <p>
            <h6>
              Counter: {this.state.count}
            </h6>
            <h6>
              Counter 2: <br />{this.state.count2}
            </h6>

            <button onClick={() => this.newIncrement()}>Increment</button>
            <button onClick={() => this.newDecrement()}>Decrement</button>        {/*  fonction anonyme fléchés | ont lecrire comme ceci  {this.newDecrement}*/}

            {/* <select id="lang" onChange={this.switchLang}>
              <option value="fr">Fr</option>
              <option value="en">En</option>
            </select> */}

          </p>
        </header>
      </div>
    );
  }
}

export default App;